import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { AuthFirebaseProvider } from '../providers/auth-firebase/auth-firebase';
import { resolve } from 'path';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  constructor(platform: Platform,
              statusBar: StatusBar,
              splashScreen: SplashScreen,
              public authFirebase: AuthFirebaseProvider) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
      this.sesionActiva();
    });
  }

  onLogout() {
    this.authFirebase.doLogout();
    this.nav.setRoot(LoginPage);
  }

  sesionActiva() {
    this.authFirebase.isSingIn()
    .then(response => {
      if (response) {
        this.nav.setRoot(HomePage);
      } else {
        this.nav.setRoot(LoginPage);
      }
    }
    )
    .catch(() => {
      this.nav.setRoot(LoginPage);
    }
    );
  }

}

