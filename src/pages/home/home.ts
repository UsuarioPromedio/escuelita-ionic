import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { RandomUserServiceProvider } from '../../providers/random-user-service/random-user-service';
import { UserDetailPage } from '../user-detail/user-detail';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  arrayUsr = [];

  constructor(
    public navCtrl: NavController,
    private randomUserServiceProvider: RandomUserServiceProvider
    ) {}

  ionViewDidLoad() {
    this.randomUserServiceProvider.getUsers()
      .then(
        res => {
          this.arrayUsr = res;
          console.log('array cargado');
          console.log(this.arrayUsr);
        },
        err => {
          console.error(err);
        }
      );
  }

  userDetail(event, user) {
    if(event) { event.stopPropagation(); }
    this.navCtrl.push(UserDetailPage, { user: user });
  }

  doRefresh(refresher) {
    this.randomUserServiceProvider.getUsers()
      .then(
        res => {
          this.arrayUsr = res;
          console.log('array cargado');
          console.log(this.arrayUsr);
          refresher.complete();
        },
        err => {
          console.error(err);
          refresher.complete();
        }
      );
    }
}
