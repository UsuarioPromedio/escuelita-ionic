import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { regexValidators } from '../../validators/validator';
import { AuthFirebaseProvider } from '../../providers/auth-firebase/auth-firebase';
import { HomePage } from '../home/home';
import { RegistroPage } from '../registro/registro'

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  credencialesForm: FormGroup;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private formBuilder: FormBuilder,
              private authFirebase: AuthFirebaseProvider) {
    this.credencialesForm = this.formBuilder.group({
      email: ['', Validators.compose([
        Validators.pattern(regexValidators.email),
        Validators.required
      ])
    ],
      password: ['', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])
    ]
  });
  }

  onLogIn() {
    if (this.credencialesForm.valid) {
      console.log("Campos validados.");
      let usr = this.credencialesForm.controls['email'].value; //let tiene un ciclo de vida que dura tanto como el if
      let pss = this.credencialesForm.controls['password'].value;
      this.login(usr, pss);
    }
  }

  login(user, pass) {
    this.authFirebase.doLogIn(user, pass)
    .then(
      response => {
        console.log("Usuario logueado.");
        this.navCtrl.setRoot(HomePage);
      }
    )
    .catch(
      error => console.log("Usuario no logueado.")
    )
  }

  goToRegister() {
    this.navCtrl.push(RegistroPage);
  }

  onForgotPassword() {
    console.log('ola k ase');
  }
}
