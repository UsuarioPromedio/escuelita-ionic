import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { regexValidators } from '../../validators/validator';
import { AuthFirebaseProvider } from '../../providers/auth-firebase/auth-firebase';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-registro',
  templateUrl: 'registro.html',
})
export class RegistroPage {

  credencialesForm: FormGroup;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private formBuilder: FormBuilder,
              private authFirebase: AuthFirebaseProvider) {
    this.credencialesForm = this.formBuilder.group({
      email: ['', Validators.compose([
        Validators.pattern(regexValidators.email),
        Validators.required
      ])
    ],
      password: ['', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])
    ]
  });
  }

  onRegister() {
    if (this.credencialesForm.valid) {
      console.log("Campos validados.");
      let data = this.credencialesForm.value;
      let credenciales = {
        email: data.email,
        password: data.password
      }
      this.register(credenciales);
    }
  }

  register(datos) {
    this.authFirebase.doRegister(datos)
    .then (
      response => {
        console.log("Cuenta creada.");
        this.navCtrl.setRoot(HomePage);
      }
    )
    .catch (
      error => {
        console.log(`Error al crear la cuenta: ${error.message}`);
      }
    );
  }
}
