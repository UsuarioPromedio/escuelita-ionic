import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-user-detail',
  templateUrl: 'user-detail.html',
})
export class UserDetailPage {

  user: any;
  user_data: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.user = navParams.get('user');
    this.cargarDatos(this.user);
  }

  cargarDatos(user){
    if (user) {
      this.user_data = {
        nombre : user.name.first.concat(" ",user.name.last),
        ubicacion : user.location.city.concat(", ",user.location.state),
        email : user.email,
        telefono : user.phone,
        celu : user.cell,
        foto_perfil : user.picture.large,
        descripcion : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut libero urna, fermentum in leo vel, euismod tristique mi. Maecenas quis odio nunc. Vestibulum at pulvinar turpis. Quisque dui tortor, pharetra nec fringilla non, semper ultrices mi."
      };
    } else {
      console.error('Error de carga');
    }
  }
}
