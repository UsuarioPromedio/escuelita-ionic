import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { first } from 'rxjs/operators';

@Injectable()
export class AuthFirebaseProvider {

  constructor(private angularFireAuth: AngularFireAuth) {
  }

  doLogIn(user, pass) {
    return new Promise ((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(user, pass)
      .then(
        response => {
          resolve(response);
          console.log('Se loguea response firebase');
          console.log(response);
        } //recibo en la var response y devuelvo ese valor como resuleto
      )
      .catch(
        error => {
          console.log("Usuario y/o contraseña no válidos.")
          reject(error);
        }
      )
    })
  }

  doRegister(credenciales) {
    return new Promise ((resolve, reject) => {
      firebase.auth().createUserWithEmailAndPassword(credenciales.email, credenciales.password)
      .then(
        response => {
          resolve(response);
          console.log("Se loguea response firebase");
          console.log(response);
        }
      )
      .catch(
        error => {
          console.log("Se logea error firebase");
          reject(error);
        }
      )
    })
  }

  doLogout() {
    return new Promise ((resolve, reject) => {
      this.angularFireAuth.auth.signOut()
        .then(() => {
          resolve();
          console.log(`Se loguea response firebase:`);
        })
        .catch(
          error => {
            console.log(`Se loguea error firebase: ${error}`);
            reject();
          }
        );
    });
  }

  isSingIn() {
    return this.angularFireAuth.authState.pipe(first()).toPromise();
  }

  doForgotPassword(email: string) {
    var auth = firebase.auth();
    return auth.sendPasswordResetEmail(email)
      .then (
        () => console.log("Email enviado.")
      )
      .catch(
        (error) => console.log(`Email no enviado: ${error}`)
      )
  }
}
