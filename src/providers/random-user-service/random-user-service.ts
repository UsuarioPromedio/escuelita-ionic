import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class RandomUserServiceProvider {

  API_URL = 'https://randomuser.me/api/?results=25';

  constructor(public http: HttpClient) {
  }

  getUsers() {
    return new Promise<any>((resolve,reject) => {
      this.http.get(this.API_URL)
        .subscribe(
          data => resolve(data['results']),
          err => reject(err)
        );
      }
    );
  }

/*   getUsers() {
    return new Promise<any>((resolve,reject) => {
      this.http.get(this.API_URL)
        .subscribe(
          function(data){
            resolve(data.results);
          },
          err => reject(err)
        );
      }
    );
  } */

}
